package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class WDC043_S2_A2 {

    public static void main(String[] args) {
        // prime numbers
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the index number: ");
        int indexNum = sc.nextInt();


        switch (indexNum) {
            case 0:
                System.out.println("The first prime number is: " + primeNumbers[indexNum]);
                break;
            case 1:
                System.out.println("The second prime number is: " + primeNumbers[indexNum]);
                break;
            case 2:
                System.out.println("The third prime number is: " + primeNumbers[indexNum]);
                break;
            case 3:
                System.out.println("The fourth prime number is: " + primeNumbers[indexNum]);
                break;
            case 4:
                System.out.println("The fifth prime number is: " + primeNumbers[indexNum]);
                break;
            default:
                System.out.println("You entered an invalid index number.");
        }

        // my friends
        ArrayList<String> myFriends = new ArrayList<String>();
        myFriends.add("John");
        myFriends.add("Jane");
        myFriends.add("Chloe");
        myFriends.add("Zoey");

        System.out.println("My friends are: " + myFriends);

        // inventory
        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("tootbrush", 20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
