package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;

public class ArraysLesson {

    public static void main(String[] args) {

        // Arrays = fixed/limited collections of data.
        // an array can hold 2^31 = 2,147,483,648 elements
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

        // other syntax used to declare array
        int intSample[] = new int[3];
        intSample[0] = 50;
        System.out.println(intSample[0]);

        // String array
        String[] stringArray = new String[3];
        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Joe";

        //Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};
        System.out.println(java.util.Arrays.toString(intArray2));

        System.out.println(java.util.Arrays.toString(stringArray));

        // Method used in arrays
        // sort()
        java.util.Arrays.sort(stringArray);
        System.out.println(java.util.Arrays.toString(stringArray));

        // binary search - this is used after doing the Array.sort() method. If printed it will return the index number if the item is present and if not it will return negative of the insertion number of the item.
        String searchTerm = "John";
        int binaryResult = java.util.Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);

        // Multidimensional Array
        String[][] classroom = new String[3][3];
        // first row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Nayeon";
        // second row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanii";
        // third row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(java.util.Arrays.deepToString(classroom));

        // ArrayList - resizable arrays, wherein elements can be added or removed whenever it is needed.

        // Declaration
        ArrayList<String> students = new ArrayList<>();

        // Add element
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        // Access element
        System.out.println(students.get(0));

        // Update element
        students.set(1, "George");
        System.out.println(students);

        // Remove element
        students.remove(1);
        System.out.println(students);

        // Remove all elements
        students.clear();
        System.out.println(students);

        // length of an ArrayList
        System.out.println(students.size());

        // Declaring and initializing ArrayList
        ArrayList<String> employees = new ArrayList<String>(Arrays.asList("June", "Albert"));
        System.out.println(employees);

        // Hashmaps -> key: value pair
        HashMap<String, String> employeeRole = new HashMap<>();

        // Add fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");
        System.out.println(employeeRole);

        // Retrieving field value
        System.out.println(employeeRole.get("Captain"));

        // Remove elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        // Retrieve hashmap keys/fields
        System.out.println(employeeRole.keySet());

        // HasMap with integers as values
        HashMap<String, Integer> grades = new HashMap<>();
        grades.put("English", 89);
        grades.put("Math", 93);
        System.out.println(grades);

        // HashMap with ArrayList
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));

        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println(subjectGrades);











        






    }



}
